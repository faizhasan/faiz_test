<?php

namespace Faiz\Test\Api;

/**
 * Interface GetInterface
 * @package Faiz\Test\Api
 */
interface GetInterface
{
    /**
     * @api
     * @param string $id
     * @return string
     */
    public function get($id);
}