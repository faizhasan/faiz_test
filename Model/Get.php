<?php

namespace Faiz\Test\Model;

use Faiz\Test\Api\GetInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class Get
 * @package Faiz\Test\Model
 */
class Get implements GetInterface
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;
    /**
     * @var \Magento\Framework\Event\Manager
     */
    private $manager;

    /**
     * Get constructor.
     * @param ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Event\Manager $manager
     */
    public function __construct(ObjectManagerInterface $objectManager, \Magento\Framework\Event\Manager $manager)
    {
        $this->objectManager = $objectManager;
        $this->manager = $manager;
    }

    /**
     * @api
     * @param string $id
     * @return string
     */
    public function get($id)
    {
        /* @var \Magento\Sales\Model\Order $order */
        $order = $this->objectManager->create('Magento\Sales\Model\Order');
        $order = $order->load($id);
        return $order->getCustomerEmail();
    }
}